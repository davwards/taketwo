import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("plugin.spring")
}

group = "com.davwards"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":language-support"))
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("org.junit.jupiter:junit-jupiter:5.5.2")
    testImplementation(project(":taketwo-gameplay-contracts"))
    testImplementation("org.assertj:assertj-core:3.13.2")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

tasks.register<Jar>("testJar") {
    archiveClassifier.set("testJar")
    from(sourceSets.test.get().allSource)
}

configurations {
    create("testArtifact")
}

artifacts {
    add("testArtifact", project.tasks.named("testJar").get())
}