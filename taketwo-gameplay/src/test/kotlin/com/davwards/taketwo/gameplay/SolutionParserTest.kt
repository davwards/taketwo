package com.davwards.taketwo.gameplay

import com.davwards.taketwo.gameplay.Directions.ACROSS
import com.davwards.taketwo.gameplay.Directions.DOWN
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SolutionParserTest {

    private val parser = SolutionParser()

    @Test
    fun `simple across case`() {
        assertThat(parser.parse(Solution(
                "CAT"
        ))).isEqualTo(setOf(
                setOf(
                        UndefinedCrosswordItem(
                                column = 0,
                                row = 0,
                                direction = ACROSS,
                                word = "CAT"
                        )
                )
        ))
    }

    @Test
    fun `small word square`() {
        assertThat(parser.parse(Solution(
                "IT",
                "DO"
        ))).isEqualTo(setOf(
                setOf(
                        UndefinedCrosswordItem(
                                column = 0,
                                row = 0,
                                direction = ACROSS,
                                word = "IT"
                        ),
                        UndefinedCrosswordItem(
                                column = 0,
                                row = 1,
                                direction = ACROSS,
                                word = "DO"
                        ),
                        UndefinedCrosswordItem(
                                column = 0,
                                row = 0,
                                direction = DOWN,
                                word = "ID"
                        ),
                        UndefinedCrosswordItem(
                                column = 1,
                                row = 0,
                                direction = DOWN,
                                word = "TO"
                        )
                )
        ))
    }

    @Test
    fun `disconnected puzzle`() {
        assertThat(parser.parse(Solution(
                "THIS T",
                "     H",
                "THAT E"
        ))).isEqualTo(setOf(
                setOf(
                        UndefinedCrosswordItem(
                                column = 0,
                                row = 0,
                                direction = ACROSS,
                                word = "THIS"
                        )
                ),
                setOf(
                        UndefinedCrosswordItem(
                                column = 0,
                                row = 2,
                                direction = ACROSS,
                                word = "THAT"
                        )
                ),
                setOf(
                        UndefinedCrosswordItem(
                                column = 5,
                                row = 0,
                                direction = DOWN,
                                word = "THE"
                        )
                )
        ))
    }

    @Test
    fun `complex puzzle`() {
        assertThat(parser.parse(Solution(
                "AFTER    I ",
                " A    B    ",
                " CATCHER  C",
                " ERR  E   O",
                " STY  S   Y"
        ))).isEqualTo(setOf(
                setOf(
                        UndefinedCrosswordItem(
                                column = 0,
                                row = 0,
                                direction = ACROSS,
                                word = "AFTER"
                        ),
                        UndefinedCrosswordItem(
                                column = 1,
                                row = 0,
                                direction = DOWN,
                                word = "FACES"
                        ),
                        UndefinedCrosswordItem(
                                column = 1,
                                row = 2,
                                direction = ACROSS,
                                word = "CATCHER"
                        ),
                        UndefinedCrosswordItem(
                                column = 1,
                                row = 3,
                                direction = ACROSS,
                                word = "ERR"
                        ),
                        UndefinedCrosswordItem(
                                column = 1,
                                row = 4,
                                direction = ACROSS,
                                word = "STY"
                        ),
                        UndefinedCrosswordItem(
                                column = 2,
                                row = 2,
                                direction = DOWN,
                                word = "ART"
                        ),
                        UndefinedCrosswordItem(
                                column = 3,
                                row = 2,
                                direction = DOWN,
                                word = "TRY"
                        ),
                        UndefinedCrosswordItem(
                                column = 6,
                                row = 1,
                                direction = DOWN,
                                word = "BEES"
                        )
                ),
                setOf(
                        UndefinedCrosswordItem(
                                column = 10,
                                row = 2,
                                direction = DOWN,
                                word = "COY"
                        )
                ),
                setOf(
                        UndefinedCrosswordItem(
                                column = 9,
                                row = 0,
                                direction = ACROSS,
                                word = "I"
                        )
                )
        ))
    }
}