package com.davwards.taketwo.gameplay

import com.davwards.taketwo.gameplay.InvalidCrossword.Errors.DISCONNECTED
import com.davwards.taketwo.gameplay.InvalidCrossword.Errors.INVALID_WORDS
import com.davwards.taketwo.gameplay.contracts.DummyMakeMoveOutcome
import com.davwards.taketwo.gameplay.contracts.MyTestHelper
import com.davwards.taketwo.support.language.UnorderedList.Companion.unorderedListOf
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class MakeMoveTest {

    private val wordValidator = FakeWordValidator(mapOf(
            "cat" to "meow",
            "cats" to "many meow",
            "bat" to "spooky"
    ))

    private val boardStateRepo = FakeBoardStateRepo()

    @Test
    fun `a valid solution that DOES NOT use up all a player's tiles does not change the board state`() {

        val initialBoardState = BoardState(
                playerPools = mapOf(
                        "playerA" to unorderedListOf('C', 'A', 'T', 'B', 'T', 'Q'),
                        "playerB" to unorderedListOf('B', 'A', 'D', 'O', 'G', 'X')
                ),
                centerPool = unorderedListOf('A', 'B', 'C', 'D')
        )

        val gameId = boardStateRepo.saveBoardState(initialBoardState)

        assertThat(MakeMove(boardStateRepo, wordValidator).execute(
                gameId = gameId,
                playerId = "playerA",
                solution = Solution(
                        " C ",
                        "BAT",
                        " T "
                ),
                outcome = object : DummyMakeMoveOutcome<Pair<BoardState, Crossword>> {
                    override fun gameContinues(
                            newBoardState: BoardState,
                            crossword: Crossword
                    ) = Pair(newBoardState, crossword)
                }
        )).isEqualTo(Pair(
                initialBoardState,
                Crossword(
                        solution = Solution(
                                " C ",
                                "BAT",
                                " T "
                        ),
                        words = setOf(
                                DefinedCrosswordItem(
                                        column = 1,
                                        row = 0,
                                        direction = Directions.DOWN,
                                        word = "CAT",
                                        definition = "meow"
                                ),
                                DefinedCrosswordItem(
                                        column = 0,
                                        row = 1,
                                        direction = Directions.ACROSS,
                                        word = "BAT",
                                        definition = "spooky"
                                )
                        )
                )
        ))
    }

    @Test
    fun `a valid solution that DOES use up all a player's tiles does not change the board state`() {

        val initialBoardState = BoardState(
                playerPools = mapOf(
                        "playerA" to unorderedListOf('C', 'A', 'T', 'B', 'T', 'S'),
                        "playerB" to unorderedListOf('B', 'A', 'D', 'O', 'G', 'X')
                ),
                centerPool = unorderedListOf('A', 'B', 'C', 'D')
        )

        val gameId = boardStateRepo.saveBoardState(initialBoardState)

        val (newBoardState, crossword) = MakeMove(boardStateRepo, wordValidator).execute(
                gameId = gameId,
                playerId = "playerA",
                solution = Solution(
                        " C ",
                        "BAT",
                        " T ",
                        " S "
                ),
                outcome = object : DummyMakeMoveOutcome<Pair<BoardState, Crossword>> {
                    override fun gameContinues(
                            newBoardState: BoardState,
                            crossword: Crossword
                    ) = Pair(newBoardState, crossword)
                }
        )

        assertThat(newBoardState.playerPools.getValue("playerA").size)
                .isEqualTo(initialBoardState.playerPools.getValue("playerA").size + 2)

        assertThat(newBoardState.playerPools.getValue("playerB").size)
                .isEqualTo(initialBoardState.playerPools.getValue("playerB").size + 2)

        assertThat(
                newBoardState.playerPools.values
                        .plusElement(newBoardState.centerPool)
                        .reduce { a, b -> a + b }
        ).isEqualTo(
                initialBoardState.playerPools.values
                        .plusElement(initialBoardState.centerPool)
                        .reduce { a, b -> a + b }
        )

        assertThat(boardStateRepo.findBoardState(gameId)).isEqualTo(newBoardState)
    }

    @Test
    fun `a valid solution that DOES use up all a player's tiles WHEN the central pool is empty ends the game`() {

        val initialBoardState = BoardState(
                playerPools = mapOf(
                        "playerA" to unorderedListOf('C', 'A', 'T', 'B', 'T', 'S'),
                        "playerB" to unorderedListOf('B', 'A', 'D', 'O', 'G', 'X')
                ),
                centerPool = unorderedListOf()
        )

        val gameId = boardStateRepo.saveBoardState(initialBoardState)

        val (newBoardState, winner) = MakeMove(boardStateRepo, wordValidator).execute(
                gameId = gameId,
                playerId = "playerA",
                solution = Solution(
                        " C ",
                        "BAT",
                        " T ",
                        " S "
                ),
                outcome = object : DummyMakeMoveOutcome<Pair<BoardState, String>> {
                    override fun gameOver(finalBoardState: BoardState, winner: String) =
                            finalBoardState to winner
                }
        )

        assertThat(newBoardState)
                .isEqualTo(initialBoardState)

        assertThat(winner).isEqualTo("playerA")
    }

    @Test
    fun `when specified game doesn't exist`() {
        assertThat(MakeMove(boardStateRepo, wordValidator).execute(
                gameId = "some-game-id-that-doesn't-exist",
                playerId = "playerA",
                solution = Solution(
                        " C ",
                        "BAT",
                        " T ",
                        " S "
                ),
                outcome = object : DummyMakeMoveOutcome<String> {
                    override fun noSuchGame() = "no such game invoked"
                }
        )).isEqualTo("no such game invoked")
    }

    @Test
    fun `when specified player doesn't exist`() {
        val initialBoardState = BoardState(
                playerPools = mapOf(
                        "playerA" to unorderedListOf('C', 'A', 'T', 'B', 'T', 'S'),
                        "playerB" to unorderedListOf('B', 'A', 'D', 'O', 'G', 'X')
                ),
                centerPool = unorderedListOf()
        )

        val gameId = boardStateRepo.saveBoardState(initialBoardState)

        assertThat(MakeMove(boardStateRepo, wordValidator).execute(
                gameId = gameId,
                playerId = "some-player-that-doesn't-exist",
                solution = Solution(
                        " C ",
                        "BAT",
                        " T ",
                        " S "
                ),
                outcome = object : DummyMakeMoveOutcome<String> {
                    override fun noSuchPlayer() = "no such player invoked"
                }
        )).isEqualTo("no such player invoked")
    }

    @Test
    fun `when the provided solution is inconsistent with the tiles the player should have in their pool`() {
        val initialBoardState = BoardState(
                playerPools = mapOf(
                        "playerA" to unorderedListOf('C', 'A', 'T', 'B', 'T', 'S'),
                        "playerB" to unorderedListOf('B', 'A', 'D', 'O', 'G', 'X')
                ),
                centerPool = unorderedListOf()
        )

        val gameId = boardStateRepo.saveBoardState(initialBoardState)

        assertThat(MakeMove(boardStateRepo, wordValidator).execute(
                gameId = gameId,
                playerId = "playerA",
                solution = Solution(
                        "STOLEN",
                        " I    ",
                        " L    ",
                        " E    ",
                        " S    "
                ),
                outcome = object : DummyMakeMoveOutcome<BoardState> {
                    override fun submissionIsInconsistentWithBoardState(currentBoardState: BoardState)
                        = currentBoardState
                }
        )).isEqualTo(initialBoardState)
    }

    @Test
    fun `when the solution is disconnected`() {
        val initialBoardState = BoardState(
                playerPools = mapOf(
                        "playerA" to unorderedListOf('C', 'A', 'T', 'B', 'T', 'A'),
                        "playerB" to unorderedListOf('B', 'A', 'D', 'O', 'G', 'X')
                ),
                centerPool = unorderedListOf()
        )

        val gameId = boardStateRepo.saveBoardState(initialBoardState)

        val invalidCrossword = MakeMove(boardStateRepo, wordValidator).execute(
                gameId = gameId,
                playerId = "playerA",
                solution = Solution(
                        "C B",
                        "A A",
                        "T T"
                ),
                outcome = object : DummyMakeMoveOutcome<InvalidCrossword> {
                    override fun crosswordIsInvalid(currentBoardState: BoardState, invalidCrossword: InvalidCrossword)
                            = invalidCrossword
                }
        )

        assertThat(invalidCrossword).isEqualTo(
                InvalidCrossword(
                        errors = setOf(DISCONNECTED),
                        crossword = setOf(
                                setOf(
                                        DefinedCrosswordItem(
                                                row = 0,
                                                column = 0,
                                                direction = Directions.DOWN,
                                                word = "CAT",
                                                definition = "meow"
                                        )
                                ),
                                setOf(
                                        DefinedCrosswordItem(
                                                row = 0,
                                                column = 2,
                                                direction = Directions.DOWN,
                                                word = "BAT",
                                                definition = "spooky"
                                        )
                                )
                        )
                )
        )
    }

    @Test
    fun `when some words are invalid`() {
        val initialBoardState = BoardState(
                playerPools = mapOf(
                        "playerA" to unorderedListOf('C', 'A', 'T', 'B', 'T', 'X'),
                        "playerB" to unorderedListOf('B', 'A', 'D', 'O', 'G', 'X')
                ),
                centerPool = unorderedListOf()
        )

        val gameId = boardStateRepo.saveBoardState(initialBoardState)

        val invalidCrossword = MakeMove(boardStateRepo, wordValidator).execute(
                gameId = gameId,
                playerId = "playerA",
                solution = Solution(
                        " C ",
                        "XAT",
                        " T "
                ),
                outcome = object : DummyMakeMoveOutcome<InvalidCrossword> {
                    override fun crosswordIsInvalid(currentBoardState: BoardState, invalidCrossword: InvalidCrossword)
                            = invalidCrossword
                }
        )

        assertThat(invalidCrossword).isEqualTo(
                InvalidCrossword(
                        errors = setOf(INVALID_WORDS),
                        crossword = setOf(
                                setOf(
                                        DefinedCrosswordItem(
                                                row = 0,
                                                column = 1,
                                                direction = Directions.DOWN,
                                                word = "CAT",
                                                definition = "meow"
                                        ),
                                        DefinedCrosswordItem(
                                                row = 1,
                                                column = 0,
                                                direction = Directions.ACROSS,
                                                word = "XAT",
                                                definition = null
                                        )
                                )
                        )
                )
        )
    }

    @Test
    fun `when solution is disconnected AND some words are invalid`() {
        val initialBoardState = BoardState(
                playerPools = mapOf(
                        "playerA" to unorderedListOf('C', 'A', 'T', 'A', 'T', 'X'),
                        "playerB" to unorderedListOf('B', 'A', 'D', 'O', 'G', 'X')
                ),
                centerPool = unorderedListOf()
        )

        val gameId = boardStateRepo.saveBoardState(initialBoardState)

        val invalidCrossword = MakeMove(boardStateRepo, wordValidator).execute(
                gameId = gameId,
                playerId = "playerA",
                solution = Solution(
                        "C X",
                        "A A",
                        "T T"
                ),
                outcome = object : DummyMakeMoveOutcome<InvalidCrossword> {
                    override fun crosswordIsInvalid(currentBoardState: BoardState, invalidCrossword: InvalidCrossword)
                            = invalidCrossword
                }
        )

        assertThat(invalidCrossword).isEqualTo(
                InvalidCrossword(
                        errors = setOf(INVALID_WORDS, DISCONNECTED),
                        crossword = setOf(
                                setOf(
                                        DefinedCrosswordItem(
                                                row = 0,
                                                column = 0,
                                                direction = Directions.DOWN,
                                                word = "CAT",
                                                definition = "meow"
                                        )
                                ),
                                setOf(
                                        DefinedCrosswordItem(
                                                row = 0,
                                                column = 2,
                                                direction = Directions.DOWN,
                                                word = "XAT",
                                                definition = null
                                        )
                                )
                        )
                )
        )
    }

    @Test
    fun `it works`() {
        assertThat(MyTestHelper().emptyCrosswordSubmission().rows).isEmpty()
    }
}

