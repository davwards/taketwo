package com.davwards.taketwo.gameplay

interface BoardStateRepo {
    fun findBoardState(gameId: String): BoardState?
    fun updateBoardState(gameId: String, newBoardState: BoardState): BoardState
    fun saveBoardState(boardState: BoardState): String
}
