package com.davwards.taketwo.gameplay

class FakeWordValidator (val dictionary: Map<String, String>): WordValidator {
    override fun define(word: String): String? {
        return dictionary[word.toLowerCase()]
    }
}
