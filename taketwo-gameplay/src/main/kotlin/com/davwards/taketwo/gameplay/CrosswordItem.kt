package com.davwards.taketwo.gameplay

interface CrosswordItem {
    val column: Int
    val row: Int
    val direction: Directions
    val word: String
}

data class DefinedCrosswordItem(
        override val column: Int,
        override val row: Int,
        override val direction: Directions,
        override val word: String,
        val definition: String?
): CrosswordItem

data class UndefinedCrosswordItem(
        override val column: Int,
        override val row: Int,
        override val direction: Directions,
        override val word: String
): CrosswordItem {
    fun withDefinition(definition: String?): DefinedCrosswordItem {
        return DefinedCrosswordItem(
                column = this.column,
                row = this.row,
                direction = this.direction,
                word = this.word,
                definition = definition
        )
    }
}
