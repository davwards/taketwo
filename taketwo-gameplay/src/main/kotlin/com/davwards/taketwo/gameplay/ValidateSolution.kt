package com.davwards.taketwo.gameplay

import com.davwards.taketwo.gameplay.InvalidCrossword.Errors
import com.davwards.taketwo.gameplay.InvalidCrossword.Errors.DISCONNECTED
import com.davwards.taketwo.gameplay.InvalidCrossword.Errors.INVALID_WORDS

class ValidateSolution(private val wordValidator: WordValidator) {
    companion object {
        interface Outcome<T> {
            fun solutionIsInvalid(invalidCrossword: InvalidCrossword): T
            fun solutionIsValid(validatedCrossword: Crossword): T
        }
    }

    private val validations: Map<Errors, ErrorCondition> = mapOf(
            INVALID_WORDS to { crossword ->
                crossword.flatten().any { it.definition == null }
            },
            DISCONNECTED to { crossword ->
                crossword.size > 1
            }
    )

    fun <T> execute(
            solution: Solution,
            outcome: Outcome<T>
    ): T {
        val definedCrossword = getWordDefinitions(solution)

        val errors = validations
                .filter { (_, condition) -> condition(definedCrossword) }
                .keys

        return if (errors.isEmpty()) {
            outcome.solutionIsValid(Crossword(
                    solution,
                    definedCrossword.first()
            ))
        } else {
            outcome.solutionIsInvalid(InvalidCrossword(
                    errors = errors,
                    crossword = definedCrossword
            ))
        }
    }

    private fun getWordDefinitions(solution: Solution): Set<Set<DefinedCrosswordItem>> {
        return SolutionParser().parse(solution)
                .map { wordGroup ->
                    wordGroup.map {
                        it.withDefinition(wordValidator.define(it.word))
                    }.toSet()
                }.toSet()
    }
}

private typealias ErrorCondition = (Set<Set<DefinedCrosswordItem>>) -> Boolean