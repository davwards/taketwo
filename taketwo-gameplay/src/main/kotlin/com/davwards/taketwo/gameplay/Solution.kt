package com.davwards.taketwo.gameplay

import com.davwards.taketwo.support.language.UnorderedList
import com.davwards.taketwo.support.language.unordered

data class Solution(
        val rows: List<List<Char>>
) {
    constructor(vararg rows: String) :
            this(rows.map { it.toCharArray().toList() })

    fun tilesUsed(): UnorderedList<Char> {
        return rows
                .flatten()
                .filter { it != ' ' }
                .unordered()
    }
}