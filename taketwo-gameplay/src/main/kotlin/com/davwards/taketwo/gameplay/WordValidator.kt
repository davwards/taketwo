package com.davwards.taketwo.gameplay

interface WordValidator {
    fun define(word: String): String?
}
