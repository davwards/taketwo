package com.davwards.taketwo.gameplay

import com.davwards.taketwo.gameplay.Directions.ACROSS
import com.davwards.taketwo.gameplay.Directions.DOWN

class SolutionParser {
    fun parse(solution: Solution): Set<Set<UndefinedCrosswordItem>> {
        val rows = solution.rows
        val columns = rows[0]
                .indices
                .map { columnNumber -> rows.map { it[columnNumber] } }

        val acrossWords = rows
                .mapIndexed(this::wordsInRow)
                .flatten()

        val downWords = columns
                .mapIndexed(this::wordsInRow)
                .flatten()
                .invertGrid()
                .filterOutSingleCharacterWordsFoundIn(acrossWords)

        val allWords = acrossWords.filterOutSingleCharacterWordsFoundIn(downWords).toSet() + downWords.toSet()

        val connectedGroups = allWords.map { wordsReachableFrom(setOf(it), allWords) }.toSet()

        return connectedGroups.map { connectedGroup ->
            connectedGroup.map { word ->
                if (acrossWords.contains(word))
                    UndefinedCrosswordItem(
                            column = word.map { it.columnNumber }.min()!!,
                            row = word.first().rowNumber,
                            direction = ACROSS,
                            word = word.map { it.character }.joinToString("")
                    )
                else
                    UndefinedCrosswordItem(
                            column = word.first().columnNumber,
                            row = word.map { it.rowNumber }.min()!!,
                            direction = DOWN,
                            word = word.map { it.character }.joinToString("")
                    )
            }.toSet()
        }.toSet()
    }

    private fun wordsReachableFrom(reachedWords: Set<List<CharacterWithPosition>>, allWords: Set<List<CharacterWithPosition>>): Set<List<CharacterWithPosition>> {
        val unreachedWords = allWords - reachedWords
        val reachableWords = unreachedWords.filter { unreachedWord ->
            reachedWords.any { reachedWord ->
                reachedWord.intersect(unreachedWord).isNotEmpty()
            }
        }

        return if (reachableWords.isEmpty())
            reachedWords
        else
            wordsReachableFrom(reachedWords + reachableWords, allWords)
    }

    private fun wordsInRow(rowNumber: Int, row: List<Char>): List<List<CharacterWithPosition>> {
        return row
                .mapIndexed { columnNumber, character -> CharacterWithPosition(rowNumber, columnNumber, character) }
                .fold(listOf(emptyList<CharacterWithPosition>())) { listOfWords, nextCharacter ->
                    val head = listOfWords.first()
                    val tail = listOfWords.drop(1)

                    if (nextCharacter.character == ' ') {
                        if (head.isEmpty()) listOfWords
                        else listOf(emptyList<CharacterWithPosition>()) + listOfWords
                    } else {
                        listOf(head + nextCharacter) + tail
                    }
                }
                .filter { it.isNotEmpty() }
    }
}

private fun List<List<CharacterWithPosition>>.filterOutSingleCharacterWordsFoundIn(other: List<List<CharacterWithPosition>>) =
        this.filter {
            !(it.size == 1 && other.any { word -> word.contains(it[0]) })
        }

private fun List<List<CharacterWithPosition>>.invertGrid() =
        this.map { word ->
            word.map {
                CharacterWithPosition(
                        rowNumber = it.columnNumber,
                        columnNumber = it.rowNumber,
                        character = it.character
                )
            }
        }

private data class CharacterWithPosition(
        val rowNumber: Int,
        val columnNumber: Int,
        val character: Char
)
