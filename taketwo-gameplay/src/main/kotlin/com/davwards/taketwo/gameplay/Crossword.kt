package com.davwards.taketwo.gameplay

data class Crossword(val solution: Solution, val words: Set<DefinedCrosswordItem>)
