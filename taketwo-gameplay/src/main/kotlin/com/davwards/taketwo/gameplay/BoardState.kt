package com.davwards.taketwo.gameplay

import com.davwards.taketwo.support.language.UnorderedList

data class BoardState(
        val playerPools: Map<String, UnorderedList<Char>>,
        val centerPool: UnorderedList<Char>
)