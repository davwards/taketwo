package com.davwards.taketwo.gameplay

data class InvalidCrossword(
        val errors: Set<Errors>,
        val crossword: Set<Set<DefinedCrosswordItem>>
) {
    enum class Errors {
        DISCONNECTED,
        INVALID_WORDS
    }
}
