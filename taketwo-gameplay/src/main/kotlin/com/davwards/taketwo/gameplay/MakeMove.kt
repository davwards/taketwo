package com.davwards.taketwo.gameplay

import com.davwards.taketwo.support.language.UnorderedList
import com.davwards.taketwo.support.language.UnorderedList.Companion.unorderedListOf
import kotlin.random.Random
import kotlin.random.nextInt

class MakeMove(
        private val boardStateRepo: BoardStateRepo,
        private val wordValidator: WordValidator
) {
    companion object {
        interface Outcome<T> {
            fun crosswordIsInvalid(
                    currentBoardState: BoardState,
                    invalidCrossword: InvalidCrossword
            ): T

            fun submissionIsInconsistentWithBoardState(
                    currentBoardState: BoardState
            ): T

            fun noSuchGame(): T

            fun noSuchPlayer(): T

            fun gameContinues(
                    newBoardState: BoardState,
                    crossword: Crossword
            ): T

            fun gameOver(
                    finalBoardState: BoardState,
                    winner: String
            ): T
        }
    }

    fun <T> execute(
            gameId: String,
            playerId: String,
            solution: Solution,
            outcome: Outcome<T>): T {

        val boardState = boardStateRepo.findBoardState(gameId)
                ?: return outcome.noSuchGame()

        val playerPool = boardState.playerPools[playerId]
                ?: return outcome.noSuchPlayer()

        if (!solution.tilesUsed().isASubsetOf(playerPool))
            return outcome.submissionIsInconsistentWithBoardState(boardState)

        return ValidateSolution(wordValidator).execute(
                solution,
                object : ValidateSolution.Companion.Outcome<T> {

                    override fun solutionIsInvalid(
                            invalidCrossword: InvalidCrossword
                    ) = outcome.crosswordIsInvalid(boardState, invalidCrossword)

                    override fun solutionIsValid(
                            validatedCrossword: Crossword
                    ) =
                            if (playerHasUsedAllTiles(validatedCrossword)) {
                                if (boardState.centerPool.isEmpty()) {
                                    outcome.gameOver(boardState, playerId)
                                } else {
                                    outcome.gameContinues(
                                            boardStateRepo.updateBoardState(
                                                    gameId,
                                                    drawNewTilesForEachPlayer()
                                            ),
                                            validatedCrossword
                                    )
                                }
                            } else {
                                outcome.gameContinues(boardState, validatedCrossword)
                            }

                    private fun playerHasUsedAllTiles(validatedCrossword: Crossword) =
                            validatedCrossword.solution.tilesUsed() == boardState.playerPools.getValue(playerId)

                    private fun drawNewTilesForEachPlayer() =
                            boardState.playerPools.keys
                                    .fold(Pair(
                                            emptyMap<String, UnorderedList<Char>>(),
                                            boardState.centerPool
                                    )) { (draws, remaining), playerId ->
                                        remaining.draw(2).let { (draw, remainingAfterDraw) ->
                                            Pair(
                                                    draws + (playerId to draw),
                                                    remainingAfterDraw
                                            )
                                        }
                                    }.let { (playerDraws, remainingTiles) ->
                                        BoardState(
                                                boardState.playerPools.mapValues { (playerId, tiles) ->
                                                    tiles + playerDraws.getValue(playerId)
                                                },
                                                remainingTiles
                                        )
                                    }
                }
        )
    }
}

fun <E> UnorderedList<E>.draw(n: Int = 1): Pair<UnorderedList<E>, UnorderedList<E>> = if (n == 1) {
    val orderedList = this.map { it }
    val indexToRemove = Random.nextInt(orderedList.indices)
    Pair(
            unorderedListOf(orderedList[indexToRemove]),
            UnorderedList(
                    orderedList
                            .filterIndexed { index, _ -> index != indexToRemove }
            )
    )
} else {
    (0 until n).fold(unorderedListOf<E>() to this) { (drawn, remaining), _ ->
        val (newDraw, remainingAfterDraw) = remaining.draw(1)
        (drawn + newDraw) to remainingAfterDraw
    }
}
