package com.davwards.taketwo.gameplay

class FakeBoardStateRepo: BoardStateRepo {
    private var idCounter = 0
    private val contents = mutableMapOf<String, BoardState>()

    override fun findBoardState(gameId: String) = contents[gameId]

    override fun updateBoardState(gameId: String, newBoardState: BoardState): BoardState {
        contents[gameId] = newBoardState
        return newBoardState
    }

    override fun saveBoardState(boardState: BoardState): String {
        val id = idCounter.toString()
        contents[id] = boardState
        idCounter++
        return id
    }
}
