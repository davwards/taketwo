package com.davwards.taketwo.support.language

import com.davwards.taketwo.support.language.UnorderedList.Companion.unorderedListOf

class UnorderedList<E>(private val elements: Collection<E>) : Collection<E> {
    override val size = elements.size
    override fun contains(element: E) = elements.contains(element)
    override fun containsAll(elements: Collection<E>) = this.elements.containsAll(elements)
    override fun isEmpty() = elements.isEmpty()
    override fun iterator() = elements.iterator()
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnorderedList<*>

        return this.histogram() == other.histogram()
    }

    override fun hashCode(): Int {
        return elements.map { it.hashCode() }.sorted().hashCode()
    }

    operator fun plus(other: UnorderedList<E>): UnorderedList<E> {
        return UnorderedList(elements + other.elements)
    }

    private fun histogram() = elements.fold(emptyMap<E, Int>()) { counts, element ->
        counts + (element to ((counts[element] ?: 0) + 1))
    }

    fun isASubsetOf(other: UnorderedList<E>): Boolean {
        return other.containsAll(this) &&
                (this.histogram() to other.histogram()).let { (thisHistogram, thatHistogram) ->
                    thisHistogram.all { (element, count) ->
                        thatHistogram.getValue(element) >= count
                    }
                }
    }

    companion object {
        fun <T> unorderedListOf(vararg elements: T): UnorderedList<T> {
            return UnorderedList(elements.toList())
        }
    }
}

inline fun <reified T> List<T>.unordered(): UnorderedList<T> {
    return unorderedListOf(*this.toTypedArray())
}