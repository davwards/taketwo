package com.davwards.taketwo.support.language

import com.davwards.taketwo.support.language.UnorderedList.Companion.unorderedListOf
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class UnorderedListTest {

    private val a = SomeClass()
    private val b = SomeClass()
    private val c = SomeClass()

    @Test
    fun `collection behavior`() {
        assertThat(unorderedListOf(a, b))
                .contains(a)
                .contains(b)
                .doesNotContain(c)
                .hasSize(2)
                .isNotEmpty
    }

    @Test
    fun `equality test`() {
        assertThat(unorderedListOf(1,1,2))
                .isEqualTo(unorderedListOf(1,2,1))
                .isEqualTo(unorderedListOf(2,1,1))
                .isNotEqualTo(unorderedListOf(2,2,1))
    }

    @Test
    fun `union test`() {
        assertThat(unorderedListOf(1,1,2) + unorderedListOf(1,2,3))
                .isEqualTo(unorderedListOf(3,2,2,1,1,1))
    }

    @Test
    fun `hash code test`() {
        assertThat(unorderedListOf(1,1,2).hashCode())
                .isEqualTo(unorderedListOf(1,2,1).hashCode())
                .isEqualTo(unorderedListOf(2,1,1).hashCode())
                .isNotEqualTo(unorderedListOf(2,2,1).hashCode())

        assertThat(unorderedListOf(a,a,b).hashCode())
                .isEqualTo(unorderedListOf(a,b,a).hashCode())
                .isEqualTo(unorderedListOf(b,a,a).hashCode())
                .isNotEqualTo(unorderedListOf(b,b,a).hashCode())
    }

    @Test
    fun `subset test`() {
        assertThat(unorderedListOf(1,1,2).isASubsetOf(unorderedListOf(1,1,1,2))).isTrue()
        assertThat(unorderedListOf(1,1,2).isASubsetOf(unorderedListOf(1,1,2))).isTrue()
        assertThat(unorderedListOf(1,1,2).isASubsetOf(unorderedListOf(1,2))).isFalse()
    }
}

private class SomeClass