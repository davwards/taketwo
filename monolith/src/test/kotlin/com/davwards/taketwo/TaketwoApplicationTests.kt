package com.davwards.taketwo

import com.davwards.taketwo.monolith.TaketwoApplication
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [ TaketwoApplication::class ])
class TaketwoApplicationTests {

	@Test
	fun contextLoads() {
	}

}
