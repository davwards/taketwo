package com.davwards.taketwo.monolith

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TaketwoApplication

fun main(args: Array<String>) {
	runApplication<TaketwoApplication>(*args)
}
