package com.davwards.taketwo.gameplay.contracts

import com.davwards.taketwo.gameplay.Solution

class MyTestHelper {
    fun magic() = "wow"
    fun emptyCrosswordSubmission() = Solution()
}