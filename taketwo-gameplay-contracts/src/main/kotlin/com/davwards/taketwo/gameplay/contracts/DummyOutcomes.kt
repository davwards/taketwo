package com.davwards.taketwo.gameplay.contracts

import com.davwards.taketwo.gameplay.BoardState
import com.davwards.taketwo.gameplay.Crossword
import com.davwards.taketwo.gameplay.InvalidCrossword
import com.davwards.taketwo.gameplay.MakeMove

interface DummyMakeMoveOutcome<T>: MakeMove.Companion.Outcome<T> {
    override fun crosswordIsInvalid(
            currentBoardState: BoardState,
            invalidCrossword: InvalidCrossword
    ): T = throw RuntimeException("crosswordIsInvalid outcome should not have been invoked!")

    override fun submissionIsInconsistentWithBoardState(
            currentBoardState: BoardState
    ): T = throw RuntimeException("submissionIsInconsistentWithBoardState outcome should have not have been invoked!")

    override fun noSuchGame(): T = throw RuntimeException("noSuchGame outcome should not have been invoked!")

    override fun noSuchPlayer(): T = throw RuntimeException("noSuchPlayer outcome should not have been invoked!")

    override fun gameContinues(
            newBoardState: BoardState,
            crossword: Crossword
    ): T = throw RuntimeException("gameContinues outcome should have not have been invoked!")

    override fun gameOver(
            finalBoardState: BoardState,
            winner: String
    ): T = throw RuntimeException("gameOver outcome should have not have been invoked!")
}