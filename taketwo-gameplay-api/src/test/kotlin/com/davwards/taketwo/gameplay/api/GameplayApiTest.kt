package com.davwards.taketwo.gameplay.api

import com.davwards.taketwo.gameplay.contracts.MyTestHelper
import io.restassured.RestAssured.`when`
import org.hamcrest.Matchers.nullValue
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles

@SpringBootTest(
        classes = [ GameplayApiTestConfig::class ],
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ActiveProfiles("GameplayApiTest")
class GameplayApiTest {

    @LocalServerPort
    private lateinit var port: String

    @Test
    fun `it works`() {
        `when`()
                .post("http://localhost:$port/whatever")
                .then()
                .statusCode(404)
                .body(MyTestHelper().magic(), nullValue())
    }
}

@Configuration
@EnableAutoConfiguration
@Profile("GameplayApiTest")
class GameplayApiTestConfig