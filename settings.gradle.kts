rootProject.name = "taketwo"

include("monolith")

include("taketwo-gameplay")
include("taketwo-gameplay-contracts")
include("taketwo-gameplay-api")
include("language-support")
